import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  role:any;
  login=false;


  constructor(private router: Router) { }

  status: any = localStorage.getItem('isuser');

  callSignup() {
    this.router.navigateByUrl('/signup');
  }

  callLogin() {
    this.router.navigateByUrl('/login');
    console.log(this.status);
  }

  callLogout() {
    localStorage.clear();
    this.router.navigateByUrl('/view-products');
    window.location.reload()
  }
  ngOnInit(): void {
    let data = JSON.parse(localStorage.getItem('login')||"{}")
    this.role = data.loginRole;
    if(this.role=="USER")
    {
      this.login=true;
    }
    console.log("Header",this.login)
  }

}
