import { Component, OnInit } from '@angular/core';
import { EcomService } from 'src/app/services/ecom.service';

@Component({
  selector: 'app-vieworder',
  templateUrl: './vieworder.component.html',
  styleUrls: ['./vieworder.component.scss']
})
export class VieworderComponent implements OnInit {

  constructor(private apiService: EcomService) { }

  data: any;
  

  ngOnInit(): void {
    let customerdetails = JSON.parse(localStorage.getItem('customer')||"");
    console.log(customerdetails.customerId); 
    this.apiService.getOrdersByCustId(customerdetails.customerId).subscribe(response => {
      this.data = response;
      console.log(this.data);
    });
}
}
