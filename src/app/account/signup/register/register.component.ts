import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { EcomService } from 'src/app/services/ecom.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  constructor(private apiService: EcomService, private formbuilder: FormBuilder) { }

  customerForm = this.formbuilder.group({
    CustomerName : ['', Validators.required],
    CustomerAddress : ['', Validators.required],
    CustomerPhoneNumber : ['', Validators.required],
    CustomerEmailId : ['', Validators.required],
    Password : ['', Validators.required]
  });

  add()
  {
    if(this.customerForm.status == 'VALID'){
      

      let login = {
        Password : this.customerForm.value.Password,
        UserId : this.customerForm.value.CustomerEmailId,
        LoginRole : 'USER'
      }

      this.apiService.addLogin(login).subscribe(loginres => {
        let loginresponse = JSON.parse(JSON.stringify(loginres));
        let customer = {
          Test:"Random",
          LoginId : loginresponse.loginId,
          CustomerName : this.customerForm.value.CustomerName,
          CustomerAddress : this.customerForm.value.CustomerAddress,
          CustomerPhoneNumber : this.customerForm.value.CustomerPhoneNumber,
          CustomerEmailId : this.customerForm.value.CustomerEmailId,
          
        }
        console.log("test1",loginresponse);
        console.log(customer);
        this.apiService.addCustomer(customer).subscribe(customerres => {
          console.log("test",customerres)
        })
      })

      

      alert('Created successfully');
    }

    else{
      alert('Please enter all required fields.');
    }
  }

  ngOnInit(): void {
  }

}
