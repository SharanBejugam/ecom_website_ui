import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { EcomService } from 'src/app/services/ecom.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  constructor(private apiService: EcomService, private formbuilder: FormBuilder, private router: Router) { }

  logindetails: any;
  customerdetails: any;

  myForm = this.formbuilder.group({
    UserId : ['', Validators.required],
    Password : ['', Validators.required]
  });

  validate() {
    this.apiService.getLoginByUserId(this.myForm.value.UserId).subscribe(data => {
      this.logindetails = data;
      localStorage.setItem('login', JSON.stringify(this.logindetails));
      console.log("LoginDetails", this.logindetails);
      if(this.logindetails.loginRole == 'USER') {
        localStorage.setItem('loginRole', "false");
      }
      else {
        localStorage.setItem('loginRole', "true");
      }
     
      if(this.logindetails.userId == this.myForm.value.UserId && this.logindetails.password == this.myForm.value.Password) { 
        this.router.navigateByUrl('/view-products');
        this.apiService.getCustomerByLoginId(this.logindetails.loginId).subscribe(data => {
          this.customerdetails = data;
          localStorage.setItem('customer',JSON.stringify(this.customerdetails));
        });
      }
      else {
        alert("User not found");
      }
    })
  }

  ngOnInit(): void {
  }

}
