import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {  FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ViewproductComponent } from './product/viewproduct/viewproduct.component';
import { AddproductComponent } from './product/addproduct/addproduct.component';
import { ViewcategoryComponent } from './category/viewcategory/viewcategory.component';
import { AddcategoryComponent } from './category/addcategory/addcategory.component';



@NgModule({
  declarations: [
  
    ViewproductComponent,
       AddproductComponent,
       ViewcategoryComponent,
       AddcategoryComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ShopModule { }